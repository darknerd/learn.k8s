
## **Prerequisites**

* [Tutorial 1: Hello App](../01_gke_hello-app/README.md)

## **Tutorial**

This tutorial walks through creating loadbalancer ingress rules on Kubernetes that map directly to GLB (Google Cloud Load Balancer) through some mechanism that hooks this into GCloud.

```bash
###### STEP 0: Make the Cluster
gcloud container clusters create loadbalancedcluster
gcloud container clusters get-credentials loadbalancedcluster
###### STEP 1: Deploy the App
kubectl run web --image=gcr.io/google-samples/hello-app:1.0 --port=8080
###### STEP 2: Expose Deployment as a Service Internally
kubectl expose deployment web --target-port=8080 --type=NodePort
kubectl get service web

###### STEP 3: Create Ingress Resource
INGRESS_NAME="basic-ingress"
INGRESS_CFG_FILE=${INGRESS_NAME}.yaml
cat <<-"EOF" > ${INGRESS_CFG_FILE}
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: basic-ingress
spec:
  backend:
    serviceName: web
    servicePort: 8080
EOF
kubectl apply -f ${INGRESS_CFG_FILE}
###### STEP 4: Visit the Application
kubectl get ingress ${INGRESS_NAME}
IP=$(kubectl get ingress ${INGRESS_NAME} | \
  grep ${INGRESS_NAME} | \
  awk '{print $3}' )
curl http://${IP}

###### STEP 5 (OPTIONAL): Configuring a static IP address
# Note: This costs money to allocate a static IP address
STATIC_IP_NAME="web-static-ip"
INGRESS_NAME="basic-ingress"
INGRESS_CFG_FILE="static-ingress.yaml"
gcloud compute addresses create ${STATIC_IP_NAME} --global
# Note: purposefully renaming ingress config file
cat <<-"EOF" > ${INGRESS_CFG_FILE}
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: ${INGRESS_NAME}
  annotations:
    kubernetes.io/ingress.global-static-ip-name: ${STATIC_IP_NAME}
spec:
  backend:
    serviceName: web
    servicePort: 8080
EOF
kubectl apply -f ${INGRESS_CFG_FILE}
# Look for new IP
IP=$(kubectl get ingress ${INGRESS_NAME} | \
  grep ${INGRESS_NAME} | \
  awk '{print $3}' )
curl http://${IP}

###### STEP 6: Serving multiple applications on a Load Balancer
kubectl run web2 --image=gcr.io/google-samples/hello-app:2.0 --port=8080
kubectl expose deployment web2 --target-port=8080 --type=NodePort
INGRESS_NAME="fanout-ingress"
INGRESS_CFG_FILE=${INGRESS_NAME}.yaml

cat <<-"EOF" > ${INGRESS_CFG_FILE}
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: ${INGRESS_NAME}
spec:
  rules:
  - http:
      paths:
      - path: /*
        backend:
          serviceName: web
          servicePort: 8080
      - path: /v2/*
        backend:
          serviceName: web2
          servicePort: 8080
EOF
kubectl create -f ${INGRESS_CFG_FILE}
IP=$(kubectl get ingress ${INGRESS_NAME} | \
  grep ${INGRESS_NAME} | \
  awk '{print $3}' )
curl http://${IP}

###### CLEAN-UP
kubectl delete ingress 'basic-ingress'
kubectl delete ingress 'fanout-ingress'
gcloud compute addresses delete 'web-static-ip' --global
gcloud container clusters delete loadbalancedcluster
```

## **References**

* https://cloud.google.com/kubernetes-engine/docs/tutorials/http-balancer
