# Guestbook (PHP, Redis)

## **Prerequisites**

* `git`, `docker`, `kubectl`, `gcloud`

### **Kubectl with GCloud**

```bash
gcloud components install kubectl # installs 1.8.6
```

### **Kubectl with Homebrew**

```bash
brew install kubectl # installs 1.10.x
```

### **Kubectl (General)**

* https://kubernetes.io/docs/tasks/tools/install-kubectl


## **Tutorial**

```bash
###### STEP 0: Build the Cluster
gcloud container clusters create guestbook --num-nodes=3
# gather info on your clusters
gcloud container clusters list
gcloud container clusters describe guestbook
# only if using existing kube engine or created cluster through console
gcloud container clusters get-credentials guestbook

###### STEP 1: Create Redis Master

kubectl create -f guestbook/redis-master-deployment.yaml
# check results
kubectl get pods
# see logs for pod just created
kubectl logs -f $(kubectl get pods | grep 'redis-master' | awk '{print $1}')

# expose service
kubectl create -f guestbook/redis-master-service.yaml
# check status of service
kubectl get service

###### STEP 2: Create Redis Workers
kubectl create -f guestbook/redis-slave-deployment.yaml
kubectl get pods
kubectl create -f guestbook/redis-slave-service.yaml
kubectl get service

###### STEP 3: Setup GuestBook Web FE
kubectl create -f guestbook/frontend-deployment.yaml
kubectl get pods -l app=guestbook -l tier=frontend
# Create local service config w/ LoadBalancer instead of default ClusterIP
cat <<-"EOF" > lb-frontend-service.yaml
apiVersion: v1
kind: Service
metadata:
  name: frontend
  labels:
    app: guestbook
    tier: frontend
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: guestbook
    tier: frontend
EOF
# Apply the service configuration
kubectl create -f lb-frontend-service.yaml

###### STEP 4: Scaling Up Web FE
kubectl scale deployment frontend --replicas=5
kubectl get pods

###### STEP 4: Scaling Up Web FE
kubectl delete service frontend
gcloud compute forwarding-rules list
gcloud container clusters delete guestbook
```
