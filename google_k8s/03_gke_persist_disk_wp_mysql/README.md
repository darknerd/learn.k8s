## **Prerequisites**

* `git`, `docker`, `kubectl`, `gcloud`

### **Kubectl with GCloud**

```bash
gcloud components install kubectl # installs 1.8.6
```

### **Kubectl with Homebrew**

```bash
brew install kubectl # installs 1.10.x
```

### **Kubectl (General)**

* https://kubernetes.io/docs/tasks/tools/install-kubectl


## **Tutorial**

```bash
###### STEP 1: Make the Cluster
gcloud container clusters create persistent-disk-tutorial --num-nodes=3
gcloud container clusters get-credentials persistent-disk-tutorial

###### STEP 2: Create PersistentVolumes and PersistentVolumeClaims
kubectl apply -f wordpress-persistent-disks/mysql-volumeclaim.yaml
kubectl apply -f wordpress-persistent-disks/wordpress-volumeclaim.yaml

###### STEP 3: Setup MySQL
kubectl create secret generic 'mysql' --from-literal=password='iciDEOuStRIp'
# Deployment Configuration will reference this password 'mysql' and store it
# as MYSQL_ROOT_PASSWORD consumed by the container.
#    - name: MYSQL_ROOT_PASSWORD
#      valueFrom:
#        secretKeyRef:
#          name: mysql
#          key: password
#
# Deploy Pod
kubectl create -f wordpress-persistent-disks/mysql.yaml
# Test that Pod is Running
kubectl get pod -l app=mysql
# Expose MySQL service
kubectl create -f wordpress-persistent-disks/mysql-service.yaml
# Test is Service is running
kubectl get service mysql

###### STEP 4: Setup WordPress
# Deployment Configuration will reference this password 'mysql' and store it
# as WORDPRESS_DB_PASSWORD consumed by the container.
kubectl create -f wordpress-persistent-disks/wordpress.yaml
# Test that Pod is Running
kubectl get pod -l app=wordpress
# Expose WordPress service
kubectl create -f wordpress-persistent-disks/wordpress-service.yaml
# Test WordPress service
kubectl get svc -l app=wordpress

###### STEP 5: Visit New Blog
IP=$(kubectl get svc -l app=wordpress | grep wordpress | awk '{print $4}')

###### STEP 6 (OPTIONAL): Test Data Persistence on Failure
kubectl get pods -o=wide
kubectl delete pod -l app=mysql
kubectl get pods -o=wide

###### STEP 7: Updating Application Images
IMAGE='wordpress:4.9.6-php5.6-apache'
cat <<-"EOF" > new_wordpress.yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  replicas: 1
  selector:
    matchLabels:
      app: wordpress
  template:
    metadata:
      labels:
        app: wordpress
    spec:
      containers:
        - image: ${IMAGE}
          name: wordpress
          env:
          - name: WORDPRESS_DB_HOST
            value: mysql:3306
          - name: WORDPRESS_DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: mysql
                key: password
          ports:
            - containerPort: 80
              name: wordpress
          volumeMounts:
            - name: wordpress-persistent-storage
              mountPath: /var/www/html
      volumes:
        - name: wordpress-persistent-storage
          persistentVolumeClaim:
            claimName: wordpress-volumeclaim
EOF
kubectl apply -f new_wordpress.yaml

###### CLEANUP
kubectl delete service wordpress
gcloud compute forwarding-rules list
kubectl delete pvc wordpress-volumeclaim
kubectl delete pvc mysql-volumeclaim
gcloud container clusters delete persistent-disk-tutorial
```

## **References**

* https://cloud.google.com/kubernetes-engine/docs/tutorials/persistent-disk
