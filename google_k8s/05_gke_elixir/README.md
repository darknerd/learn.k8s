# Elixir Phoenix app in containers using GKE

## **Prerequisites**

* `git`, `docker`, `kubectl`, `gcloud`, `elixir`

### **Kubectl with GCloud**

```bash
gcloud components install kubectl # installs 1.8.6
```

### **Kubectl with Homebrew**

```bash
brew install kubectl # installs 1.10.x
```

### **Kubectl (General)**

* https://kubernetes.io/docs/tasks/tools/install-kubectl

### **Elixir with Homebrew**

```bash
brew install elixir
mix local.hex
mix local.rebar
mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez
```

### **Node with Homebrew**

I prefer to use [NVM](https://github.com/creationix/nvm) in my dev environments, but for those needing quick and easy way to install NodeJS:

```bash
brew install node
```


# **Local Project**


## **Build Hello App**


```bash
###############################################################
#  Create A New App
###############################################################
mix phx.new hello --no-ecto
cd hello
mix phx.server
curl -i http://localhost:4000
```

## **Building and Packaging Application**

```bash
###############################################################
#  Setup Distillery
###############################################################
# Add distillery to your application's dependencies
sed -i '/{:phoenix,/i \      {:distillery, "~> 1.5"},' mix.exs
mix do deps.get, deps.compile
# Default release configuration by running
mix release.init
# Prepare the Phoenix configuration for deployment
sed -i '/config :hello, HelloWeb.Endpoint/a\  load_from_system_env: true,\
  http: [port: "${PORT}"],\
  check_origin: false,\
  server: true,\
  root: ".",\
  cache_static_manifest: "priv/static/cache_manifest.json",' config/config.exs

###############################################################
#  Test a Release
###############################################################

# Build and digest the application assets for production:
cd assets
npm i -g npm
npm install
./node_modules/brunch/bin/brunch build -p
cd ..
mix phx.digest
# Build Release
MIX_ENV=prod mix release --env=prod
# Run the application from the release using
PORT=8080 _build/prod/rel/hello/bin/hello foreground
```

## **Dockerizing Application**

```bash
###############################################################
#  Create Docker files
###############################################################
cat <<-'DOCKERFILE' > Dockerfile
FROM elixir:alpine
ARG APP_NAME=hello
ARG PHOENIX_SUBDIR=.
ENV MIX_ENV=prod REPLACE_OS_VARS=true TERM=xterm
WORKDIR /opt/app
RUN apk update \
    && apk --no-cache --update add nodejs nodejs-npm \
    && mix local.rebar --force \
    && mix local.hex --force
COPY . .
RUN mix do deps.get, deps.compile, compile
RUN cd ${PHOENIX_SUBDIR}/assets \
    && npm install \
    && ./node_modules/brunch/bin/brunch build -p \
    && cd .. \
    && mix phx.digest
RUN mix release --env=prod --verbose \
    && mv _build/prod/rel/${APP_NAME} /opt/release \
    && mv /opt/release/bin/${APP_NAME} /opt/release/bin/start_server

FROM alpine:latest
RUN apk update && apk --no-cache --update add bash openssl-dev
ENV PORT=8080 MIX_ENV=prod REPLACE_OS_VARS=true
WORKDIR /opt/app
EXPOSE ${PORT}
COPY --from=0 /opt/release .
CMD ["/opt/app/bin/start_server", "foreground"]
DOCKERFILE

copy <<-'DOCKERIGNORE' > .dockerignore
_build
deps
test
assets/node_modules
DOCKERIGNORE

###############################################################
#  Build the Application
###############################################################
docker build --no-cache -t hello .

###############################################################
#  Test the Results Locally
###############################################################
# Run the Service
docker run -it --rm -p 8080:8080 hello
# In another terminal curl it
WEBHOST_IP=${"$(docker-machine ip)":-"127.0.0.1"}
curl $WEBHOST_IP:8080
```

## **Deploying Application**

```bash
###############################################################
#  Deploying Application
###############################################################
PROJECT_ID=$(gcloud config list --format 'value(core.project)')

# GCR ignores dockerignore and uses gitignore
# WARNING: Don't do this in real production, this is for demo only
sed -i 's|/config/*.secret.exs|#/config/*.secret.exs|' .gitignore
gcloud container builds submit --tag=gcr.io/${PROJECT_ID}/hello:v1 .
gcloud container images list
```

## **Create GKE Cluster**

```bash
# Set Region if not set
gcloud config set compute/zone us-east1-b
# Enable GKE API locally if not enabled already
gcloud services enable container.googleapis.com

###############################################################
# Create Cluster
###############################################################
gcloud container clusters create hello-cluster --num-nodes=2
# This this cluster to the default
gcloud config set container/cluster hello-cluster
```

## **Deploy to GKE Cluster**

```bash
###############################################################
# Deploy to Cluster
###############################################################
# Create a Deployment
kubectl run hello-web --image=gcr.io/${PROJECT_ID}/hello:v1 --port 8080
# view the running pods
kubectl get pods
# Expose the application by creating a load balancer
kubectl expose deployment hello-web --type=LoadBalancer --port 80 --target-port 8080
# Check to see when EXTERNAL-IP is available and assigned
WEBHOST_IP=$(kubectl get service | grep hello-web | awk '{print $4}')

###############################################################
# Scaling use case demo
###############################################################
kubectl scale deployment hello-web --replicas=3
kubectl get pods

###############################################################
# Update use case demo
###############################################################
gcloud container builds submit --tag=gcr.io/${PROJECT_ID}/hello:v2 .
# Set the deployment to use the new image
kubectl set image deployment/hello-web hello-web=gcr.io/${PROJECT_ID}/hello:v2

###############################################################
# Rollback use case demo
###############################################################
kubectl set image deployment/hello-web hello-web=gcr.io/${PROJECT_ID}/hello:v1
```

## **Cleanup**

```bash
###############################################################
# Cleanup
###############################################################
kubectl delete service hello-web
gcloud compute forwarding-rules list
gcloud container clusters delete hello-cluster
```
