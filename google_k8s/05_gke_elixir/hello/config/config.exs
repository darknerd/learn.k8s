# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :hello, HelloWeb.Endpoint,
  load_from_system_env: true,
  http: [port: "${PORT}"],
  check_origin: false,
  server: true,
  root: ".",
  url: [host: "localhost"],
  cache_static_manifest: "priv/static/cache_manifest.json",
  secret_key_base: "bj/pUYRvkcXcKMnlaljepaE0DMpsxLUqi6KKFHxqv+TOA9uJqtJUokwVh9ByTbGG",
  render_errors: [view: HelloWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Hello.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
