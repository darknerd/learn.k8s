# Hello App


## **Prerequisites**

* `git`, `docker`, `kubectl`, `gcloud`

### **Kubectl with GCloud**

```bash
gcloud components install kubectl # installs 1.8.6
```

### **Kubectl with Homebrew**

```bash
brew install kubectl # installs 1.10.x
```

### **Kubectl (General)**

* https://kubernetes.io/docs/tasks/tools/install-kubectl


## **Tutorial**

```bash
###### STEP 1: Build the Container
export PROJECT_ID="$(gcloud config get-value project -q)"
cd hello-app # symlinked to submodule - git submodule init
docker build -t gcr.io/${PROJECT_ID}/hello-app:v1 .

###### STEP 2: Upload The Container Image
gcloud docker -- push gcr.io/${PROJECT_ID}/hello-app:v1

###### STEP 3: Run it Locally (optional)
docker run --rm -p 8080:8080 gcr.io/${PROJECT_ID}/hello-app:v1
curl http://localhost:8080  # curl http://$(docker-machine ip):8080
curl http://localhost:8080/this/is/a/path  # curl http://$(docker-machine ip):8080/this/is/a/path

###### STEP 4: Create a Container Cluster
gcloud container clusters create hello-cluster --num-nodes=3
gcloud compute instances list
# Get Kredentials (for your kubectl configuration)
gcloud container clusters get-credentials hello-cluster

###### STEP 5: Deploy Your Application
kubectl run hello-web --image=gcr.io/${PROJECT_ID}/hello-app:v1 --port 8080
kubectl get pods

###### STEP 6: Expose your Application to the Internet (Weeeeeee)
kubectl expose deployment hello-web --type=LoadBalancer --port 80 --target-port 8080
# get external IP address to the Service resource
kubectl get service

###### STEP 7: Scale Up Your Application
kubectl scale deployment hello-web --replicas=3
kubectl get deployment hello-web
kubectl get pods

###### STEP 8: Deploy a New Version of Your Application
docker build -t gcr.io/${PROJECT_ID}/hello-app:v2 .
gcloud docker -- push gcr.io/${PROJECT_ID}/hello-app:v2
kubectl set image deployment/hello-web hello-web=gcr.io/${PROJECT_ID}/hello-app:v2

###### CLEAN UP
kubectl delete service hello-web
# wait until it is deleted 
gcloud compute forwarding-rules list
gcloud container clusters delete hello-cluster

```

## Troubleshooting

### **Enabling Google Container Registry API**

```bash
# Some message like this
"denied: Token exchange failed for project '${PROJECT_ID}'. Please enable Google Container Registry API in Cloud Console at https://console.cloud.google.com/apis/api/containerregistry.googleapis.com/overview?project=${PROJECT_ID} before performing this operation."
```

Solution(s):
* https://console.cloud.google.com/apis/library/containerregistry.googleapis.com/


### **Unsupported Docker Version**

```
WARNING: `gcloud docker` will not be supported for Docker client versions above 18.03.
```

Solution(s):
 * Unknown at this point

## References

* https://cloud.google.com/kubernetes-engine/docs/tutorials/hello-app
