# Learn Kubernetes

This project walks through various tutorials online to learn Kubernetes.


## **GKE Tutorials**

* https://cloud.google.com/kubernetes-engine/docs/tutorials/
    * https://cloud.google.com/kubernetes-engine/docs/tutorials/persistent-disk
    * GuestBook App (PHP): https://cloud.google.com/kubernetes-engine/docs/tutorials/guestbook
    * BookShelf Application
        * Go: https://cloud.google.com/go/docs/tutorials/bookshelf-on-kubernetes-engine
        * Python: https://cloud.google.com/python/tutorials/bookshelf-on-kubernetes-engine
        * Ruby: https://cloud.google.com/ruby/tutorials/bookshelf-on-kubernetes-engine
        * NodeJS: https://cloud.google.com/nodejs/docs/tutorials/bookshelf-on-kubernetes-engine
    * Django Application (Python): https://cloud.google.com/python/django/kubernetes-engine
    * MicroServices Frameworks (Istio, Envoy): https://cloud.google.com/kubernetes-engine/docs/tutorials/installing-istio
* Community Tutorials:
    * [Deploying Envoy with a Python Flask webapp and Google Kubernetes Engine](https://cloud.google.com/community/tutorials/envoy-flask-google-container-engine)
    * [Transparent Proxy and Filtering on Kubernetes](https://cloud.google.com/community/tutorials/transparent-proxy-and-filtering-on-k8s)
    * [Transparent Proxy and Filtering on Kubernetes with Initializers](https://cloud.google.com/community/tutorials/transparent-proxy-and-filtering-on-k8s-with-initializers)
    * [Ingress with NGINX controller on Google Kubernetes Engine](https://cloud.google.com/community/tutorials/nginx-ingress-gke)
    * [Run an Elixir Phoenix app in containers using Google Kubernetes Engine](https://cloud.google.com/community/tutorials/elixir-phoenix-on-kubernetes-google-container-engine)
    * [Run a Kotlin Spring Boot application on Google Kubernetes Engine](https://cloud.google.com/community/tutorials/kotlin-springboot-container-engine)
    * [Locally developing microservices with Google Kubernetes Engine](https://cloud.google.com/community/tutorials/developing-services-with-k8s)
    * [](https://docs.aws.amazon.com/ja_jp/eks/latest/userguide/getting-started.html)

## **EKS Tutorials**

* [Getting Started with Amazon EKS](https://aws.amazon.com/eks/getting-started/)
* [Tutorial: Deploy the Kubernetes Web UI (Dashboard)](https://docs.aws.amazon.com/eks/latest/userguide/dashboard-tutorial.html)
* [Tutorial: Creating a VPC with Public and Private Subnets for Your Amazon EKS Cluster](https://docs.aws.amazon.com/eks/latest/userguide/create-public-private-vpc.html)
* [Getting Started with Amazon EKS](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html)
* [Getting Started with AWS EKS](https://www.terraform.io/docs/providers/aws/guides/eks-getting-started.html)

## **Interesting Related Tech**

Google Cloud (and other cloud providers) are listed as they can be used to create initial GKE cluster, before the hand-off to Kubernetes API.

* Generate TF from InSpec
  * https://github.com/inspec/inspec-iggy
* InSpec
  * https://github.com/inspec/inspec-gcp
* Testing Kubernetes
  * General
    * https://github.com/pearsontechnology/kubernetes-tests
  * Integration Tests (InSpec)
    * https://github.com/inspec/inspec-gcp
  * Test Harness (Test Kitchen)
    * https://github.com/coderanger/kitchen-kubernetes
    * https://github.com/test-kitchen/kitchen-google
* Continious Integration
  * Jenkins
    * https://github.com/jenkinsci/kubernetes-plugin
* Infrastructure as Code
  * Terraform
    * https://www.terraform.io/docs/providers/kubernetes/index.html
    * https://www.terraform.io/docs/providers/google/index.html
  * Chef
    * https://github.com/bloomberg/kubernetes-cluster-cookbook
    * https://github.com/chef-cookbooks/kubernetes
    * https://cloudplatform.googleblog.com/2017/10/managing-google-cloud-platform-with-chef.html
    * https://github.com/GoogleCloudPlatform/magic-modules/tree/master/provider
* Service Meshes
  * https://linkerd.io/getting-started/k8s/
* Container Networks:
   * Weave
     * https://www.weave.works/docs/scope/latest/installing/#k8s
     * https://www.weave.works/features/container-network-firewalls/
