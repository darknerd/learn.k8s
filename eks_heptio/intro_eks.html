<div class="js-markdown-instructions lab-content__markdown markdown-lab-instructions no-select" id="markdown-lab-instructions">

<h1>Lab 01: Containerize and Publish Applications</h1>

<p>In this lab, you will set up your lab workstation, create and publish Docker images for a two-tier web application, and start the process of creating a Kubernetes cluster using Amazon's Elastic Container Service for Kubernetes (EKS).</p>

<h2 id="step1">Workstation Setup</h2>

<h3>Step 1: Access Your Lab Workstation</h3>

<p>There are two ways to access your lab workstation. You can use the web terminal built into Strigo by clicking the My Lab icon on the left:</p>

<p><img src="./Introduction to Containers and Kubernetes with EKS _ Qwiklabs + heptio_files/image1.png" alt="Strigo Lab Workstation"></p>

<p>Or you can issue the 'lab-info' command in the web terminal to retrieve SSH connection details.</p>

<h3>Step 2: Configure the AWS CLI</h3>

<p>We will use the AWS cli to set up authentication for EKS cluster creation, ECR authentication, and to create ECR repositories.</p>

<p>Set up your AWS credentials by running 'aws configure'. When prompted for your 'AWS Access Key ID' and 'AWS Secret Access Key', enter the values displayed in Qwiklabs console. When prompted for 'Default region name', enter 'us-west-2'.</p>

<p><input readonly="" class="copyable-inline-input" size="13" type="text" value="aws configure"></p>
<pre class="highlight plaintext"><code>AWS Access Key ID [None]: &lt;copy Access Key ID from Qwiklabs&gt;
AWS Secret Access Key [None]: &lt;copy Secret Access Key from Qwiklabs&gt;
Default region name [None]: us-west-2
Default output format [None]: &lt;leave blank&gt;
</code><button class="button button--copy js-copy-button-0"><i class="fa fa-clipboard"></i></button></pre>
<h2 id="step2">Setting Up a Container Registry</h2>

<p>Your Kubernetes cluster needs a container registry to pull images from. Here we'll use Amazon's Elastic Container Registry (ECR). We will create two repositories in ECR, one for the frontend guestbook application and one for the Redis backend.</p>

<h3>Step 1: Create an ECR repository</h3>

<p><input readonly="" class="copyable-inline-input" size="55" type="text" value="aws ecr create-repository --repository-name redis-slave">
<input readonly="" class="copyable-inline-input" size="53" type="text" value="aws ecr create-repository --repository-name guestbook"></p>

<p>Example output:</p>
<pre class="highlight plaintext"><code>{
    "repository": {
        "repositoryArn": "arn:aws:ecr:us-west-2:883456218093:repository/guestbook",
        "repositoryUri": "883456210000.dkr.ecr.us-west-2.amazonaws.com/guestbook",
        "registryId": "883456210000",
        "createdAt": 1528415168.0,
        "repositoryName": "nginx"
    }
}
</code><button class="button button--copy js-copy-button-1"><i class="fa fa-clipboard"></i></button></pre>
<p>"repositoryURI" is the URI for the repository. You will use this to push and pull your Docker images later in the lab.</p>

<h3>Step 2: Store the repository URI as an environment variable</h3>

<p>To make it easier to reference our repositories later on, assign the names to environment variables.</p>

<p>Note: Scroll the box below to the right to see the entire command.
<input readonly="" class="copyable-inline-input" size="143" type="text" value="export ECR_REDIS_REPO=$(aws ecr describe-repositories --repository-names &quot;redis-slave&quot; --query 'repositories[*].[repositoryUri]' --output text)">
<input readonly="" class="copyable-inline-input" size="145" type="text" value="export ECR_GUESTBOOK_REPO=$(aws ecr describe-repositories --repository-names &quot;guestbook&quot; --query 'repositories[*].[repositoryUri]' --output text)"></p>

<p>We can confirm the creation of these repositories by browsing to <a href="https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/repositories" target="_blank">ECR in the Amazon Console</a></p>

<h3>Step 3: Setup Docker to ECR authentication</h3>

<p>We need to connect Docker to our AWS account. Use the following aws cli command to login. Be sure to include the ‘$’ at the beginning.</p>

<p><input readonly="" class="copyable-inline-input" size="39" type="text" value="$(aws ecr get-login --no-include-email)"></p>

<p>Expected output:</p>
<pre class="highlight plaintext"><code>Login Succeeded
</code><button class="button button--copy js-copy-button-2"><i class="fa fa-clipboard"></i></button></pre>
<h2 id="step3">Build and Publish Docker Image for the Backend Application</h2>

<h3>Step 1: Fetch the lab files</h3>

<p>We have prepared an archive with the various files we will be using for the lab. Let’s fetch a copy and unpack it.</p>

<p>Note: Scroll the box below to the right to see the entire command.
<input readonly="" class="copyable-inline-input" size="81" type="text" value="wget https://s3-us-west-2.amazonaws.com/heptio-training/k8sintro/guestbook.tar.gz">
<input readonly="" class="copyable-inline-input" size="26" type="text" value="tar -zxvf guestbook.tar.gz"></p>

<h3>Step 2: Build redis-slave Docker image</h3>

<p>We've provided a complete Dockerfile that you'll use to build the image. Feel free to view the Dockerfile to see what is being run.</p>

<p><input readonly="" class="copyable-inline-input" size="30" type="text" value="cd $HOME/guestbook/redis-slave"></p>

<p>Build and tag the image. Do not forget the "." at the end.</p>

<p><input readonly="" class="copyable-inline-input" size="32" type="text" value="docker build -t redis-slave:v1 ."></p>

<p>Watch the output of this command and notice the various stages that happen during the build process of an image. Next we will add another tag to the image. This will allow us to push the image to ECR</p>

<p><input readonly="" class="copyable-inline-input" size="46" type="text" value="docker tag redis-slave:v1 ${ECR_REDIS_REPO}:v1"></p>

<p>We can then verify this applied by listing the images. Notice how even those we have two entries, the IMAGE ID is the same for both.</p>

<p><input readonly="" class="copyable-inline-input" size="13" type="text" value="docker images"></p>
<pre class="highlight plaintext"><code>$ docker images
REPOSITORY                                                 TAG                 IMAGE ID            CREATED              SIZE
908357542759.dkr.ecr.us-west-2.amazonaws.com/redis-slave   v1                  c3ec44a1eac5        About a minute ago   107 MB
redis-slave                                                v1                  c3ec44a1eac5        About a minute ago   107 MB
redis                                                      latest              bfcb1f6df2db        5 weeks ago          107 MB
</code><button class="button button--copy js-copy-button-3"><i class="fa fa-clipboard"></i></button></pre>
<h3>Step 3: Push the Redis image to the Elastic Container Registry (ECR)</h3>

<p>Since we already added the tag and connected Docker to ECR, we can simply run 'docker push image_name' and our image will be uploaded to ECR.</p>

<p><input readonly="" class="copyable-inline-input" size="29" type="text" value="docker push ${ECR_REDIS_REPO}"></p>

<h3>Step 4: Verify the image has been published to the registry</h3>

<p>We can browse to the <a href="https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/repositories/redis-slave#images;tagStatus=ALL" target="_blank">ECR repo in the Amazon Console</a> and verify that it was uploaded:</p>

<p><img src="./Introduction to Containers and Kubernetes with EKS _ Qwiklabs + heptio_files/image2.png" alt="Redis in ECR"></p>

<h2 id="step4">Build and Publish Docker Image for the Frontend Application</h2>

<p>We are now going to repeat image creation and publishing process, this time for the PHP guestbook application. Many of the same principles and explanations apply here. The Dockerfile will be different however so make sure to take a look at it.</p>

<h3>Step 1: Build the Guestbook PHP application Docker image</h3>

<p><input readonly="" class="copyable-inline-input" size="28" type="text" value="cd $HOME/guestbook/guestbook"></p>

<p>Build and tag the image. Do not forget the "." at the end.</p>

<p><input readonly="" class="copyable-inline-input" size="30" type="text" value="docker build -t guestbook:v1 ."></p>

<h3>Step 2: Push the Guestbook image to the Elastic Container Registry (ECR)</h3>

<p>Just like before, we will add another tag to the image. This will allow us to push the image to ECR.</p>

<p><input readonly="" class="copyable-inline-input" size="48" type="text" value="docker tag guestbook:v1 ${ECR_GUESTBOOK_REPO}:v1"></p>

<p>And now push the image to ECR.</p>

<p><input readonly="" class="copyable-inline-input" size="33" type="text" value="docker push ${ECR_GUESTBOOK_REPO}"></p>

<h3>Step 3: Verify the image has been published to the registry</h3>

<p>We can browse to <a href="https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/repositories/guestbook#images;tagStatus=ALL" target="_blank">ECR repo in the Amazon Console</a> and verify that it was uploaded</p>

<p><img src="./Introduction to Containers and Kubernetes with EKS _ Qwiklabs + heptio_files/image3.png" alt="Guestbook in ECR"></p>

<p>Now that you have uploaded your images to ECR, your Kubernetes cluster will be able to pull them when we deploy the application in the next lab.</p>

<h2 id="step5">Create a Kubernetes Cluster using Amazon Elastic Container Service for Kubernetes (EKS)</h2>

<p>So far, we have not used Kubernetes. We have containerized the applications and published images to a container registry (ECR) for later use. In lab 02 we will deploy our application to Kubernetes. To conclude lab 01, we'll start the EKS cluster creation process so that it can run while we continue with the course.</p>

<h3>Step 1: Create an SSH Keypair for EKS cluster nodes</h3>

<p>Create a ssh keypair that will be used for the Kubernetes worker nodes.</p>

<p><input readonly="" class="copyable-inline-input" size="36" type="text" value="ssh-keygen -q -N &quot;&quot; -f ~/.ssh/id_rsa"></p>

<p>Example output:</p>
<pre class="highlight plaintext"><code>Generating public/private rsa key pair.
Enter file in which to save the key (/home/ubuntu/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/ubuntu/.ssh/id_rsa.
Your public key has been saved in /home/ubuntu/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:Sw46wvE8V7iDwXJrOM1+ceRkgvmMs+/hEbZLIDxIuYE ubuntu@ip-172-31-47-156
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|. .              |
|E+   o           |
|.oo.o ..+        |
|..* =+=*S        |
| . %oB+Oo.       |
|  = %oOoo        |
|   =.*.=         |
|    .+=          |
+----[SHA256]-----+
</code><button class="button button--copy js-copy-button-4"><i class="fa fa-clipboard"></i></button></pre>
<h3>Step 2: Create the cluster</h3>

<p>For this class, we will use the <a href="https://github.com/weaveworks/eksctl" target="_blank">eksctl command-line tool</a> to setup our EKS cluster. For an in depth look at the EKS cluster creation process, check out this <a href="https://aws.amazon.com/blogs/aws/amazon-eks-now-generally-available/" target="_blank">EKS blog post</a>.</p>

<p>Note: Scroll the box below to the right to see the entire command.
<input readonly="" class="copyable-inline-input" size="82" type="text" value="eksctl create cluster --cluster-name eks-cluster-1 --kubeconfig $HOME/.kube/config"></p>

<p>Example output:</p>
<pre class="highlight plaintext"><code>2018-06-07T20:53:53Z [ℹ]  importing SSH public key "/home/ubuntu/.ssh/id_rsa.pub" as "EKS-floral-gopher-1528404833"
2018-06-07T20:53:54Z [ℹ]  creating EKS cluster "floral-gopher-1528404833"
2018-06-07T20:53:54Z [ℹ]  creating VPC stack "EKS-floral-gopher-1528404833-VPC"
2018-06-07T20:53:54Z [ℹ]  creating ServiceRole stack "EKS-floral-gopher-1528404833-ServiceRole"
2018-06-07T20:54:35Z [✔]  created ServiceRole stack "EKS-floral-gopher-1528404833-ServiceRole"
2018-06-07T20:54:54Z [✔]  created VPC stack "EKS-floral-gopher-1528404833-VPC"
2018-06-07T20:54:54Z [ℹ]  creating control plane "floral-gopher-1528404833"
2018-06-07T21:03:57Z [✔]  created control plane "floral-gopher-1528404833"
2018-06-07T21:03:57Z [ℹ]  creating DefaultNodeGroup stack "EKS-floral-gopher-1528404833-DefaultNodeGroup"
2018-06-07T21:07:38Z [✔]  created DefaultNodeGroup stack "EKS-floral-gopher-1528404833-DefaultNodeGroup"
2018-06-07T21:07:38Z [✔]  all EKS cluster "floral-gopher-1528404833" resources has been created
2018-06-07T21:07:38Z [ℹ]  wrote "kubeconfig"
2018-06-07T21:07:39Z [ℹ]  the cluster has 0 nodes
2018-06-07T21:07:39Z [ℹ]  waiting for at least 2 nodes to become ready
2018-06-07T21:08:10Z [ℹ]  the cluster has 2 nodes
2018-06-07T21:08:10Z [ℹ]  node "ip-192-168-255-151.us-west-2.compute.internal" is ready
2018-06-07T21:08:10Z [ℹ]  node "ip-192-168-78-184.us-west-2.compute.internal" is ready
</code><button class="button button--copy js-copy-button-5"><i class="fa fa-clipboard"></i></button></pre>
<p>The above command will take about 15 minutes to complete. While it is executing you will see the various actions being taken and can view them in the AWS Console. For example:</p>
<ul>
<li><a href="https://console.aws.amazon.com/iam/home?region=us-west-2#/roles" target="_blank">Roles</a></li>
<li><a href="https://us-west-2.console.aws.amazon.com/vpc/home?region=us-west-2#vpcs:" target="_blank">VPC</a></li>
<li><a href="https://us-west-2.console.aws.amazon.com/eks/home?region=us-west-2#/" target="_blank">EKS Cluster</a></li>
</ul>
<p>Allow the cluster creation process to complete while we move forward with the lecture on Kubernetes. We will return here for Lab 02 following the lecture.</p>

<h1>Lab 02 - Deploy Applications using Kubernetes</h1>

<h2 id="step6">Deploy Redis using Kubernetes</h2>

<h3>Step 1: make sure the cluster is up and healthy</h3>

<p>At this point, we're ready for deployment using Kubernetes. Access to kubernetes is primarily done via the <a href="https://kubernetes.io/docs/reference/kubectl/overview/" target="_blank">command line tool kubectl</a> which has already been installed on the remote you’re logged into. <input readonly="" class="copyable-inline-input" size="7" type="text" value="kubectl"> is invoking HTTP REST JSON requests against the Kubernetes API server. It was pre-configured as the last step of the eksctl command we ran at the end of lab 01. Let’s first make sure connectivity is established by listing the available nodes in the cluster.</p>

<p><input readonly="" class="copyable-inline-input" size="17" type="text" value="kubectl get nodes"></p>

<p>Example output:</p>
<pre class="highlight plaintext"><code>NAME                                            STATUS    ROLES     AGE       VERSION
ip-192-168-188-54.us-west-2.compute.internal    Ready     &lt;none&gt;    1m        v1.10.3
ip-192-168-201-201.us-west-2.compute.internal   Ready     &lt;none&gt;    1m        v1.10.3
</code><button class="button button--copy js-copy-button-6"><i class="fa fa-clipboard"></i></button></pre>
<p>The version number may vary slightly from what is shown above. The clusters that are created use the default version for EKS. We can also look at the configuration file for kubectl that tells it how to talk to the cluster:</p>

<p><input readonly="" class="copyable-inline-input" size="19" type="text" value="kubectl config view"></p>

<p>Example output:</p>
<pre class="highlight plaintext"><code>apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: REDACTED
    server: https://F43F2B7C7A570E6ACB99167C007B502A.yl4.us-west-2.eks.amazonaws.com
  name: eks-cluster-1.us-west-2.eksctl.io
contexts:
- context:
    cluster: eks-cluster-1.us-west-2.eksctl.io
    user: awsstudent@eks-cluster-1.us-west-2.eksctl.io
  name: awsstudent@eks-cluster-1.us-west-2.eksctl.io
current-context: awsstudent@eks-cluster-1.us-west-2.eksctl.io
kind: Config
preferences: {}
users:
- name: awsstudent@eks-cluster-1.us-west-2.eksctl.io
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      args:
      - token
      - -i
      - eks-cluster-1
      command: heptio-authenticator-aws
      env: null
</code><button class="button button--copy js-copy-button-7"><i class="fa fa-clipboard"></i></button></pre>
<p>Notice the API url of the control plane and the authentication being done with the Heptio Authenticator for AWS. We can run the command manually to see that it gives kubectl a token that it can send along to the API server which is how the authentication is performed:</p>

<p><input readonly="" class="copyable-inline-input" size="47" type="text" value="heptio-authenticator-aws token -i eks-cluster-1"></p>

<h3>Step 2: create and deploy redis master Service object</h3>

<p>We are using a pre-built Redis Docker image.</p>

<p><input readonly="" class="copyable-inline-input" size="18" type="text" value="cd $HOME/guestbook"></p>

<p>We are now going to use the kubectl command to send up YAML files representing the desired state we would like Kubernetes to get the cluster to. We have pre-authored these manifest files, but please feel free to view them and research any parts of them you are curious about.</p>

<p><input readonly="" class="copyable-inline-input" size="42" type="text" value="kubectl apply -f redis-master-service.yaml"></p>

<p>Confirm redis master service is running by fetching details about we just sent into Kubernetes. If we see an entry and an IP address was created for it, then we are good!</p>

<p><input readonly="" class="copyable-inline-input" size="32" type="text" value="kubectl get service redis-master"></p>

<p>Expected sample output:</p>
<pre class="highlight plaintext"><code>NAME           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
redis-master   ClusterIP   10.100.106.40   &lt;none&gt;        6379/TCP   31s]
</code><button class="button button--copy js-copy-button-8"><i class="fa fa-clipboard"></i></button></pre>
<h3>Step 3: create and deploy redis master Deployment object</h3>

<p>We are using a pre-built Redis Docker image.</p>

<p><input readonly="" class="copyable-inline-input" size="18" type="text" value="cd $HOME/guestbook"></p>

<p>Create redis master Deployment using manifest:</p>

<p><input readonly="" class="copyable-inline-input" size="45" type="text" value="kubectl apply -f redis-master-deployment.yaml"></p>

<p>Confirm redis master Deployment is running:</p>

<p><input readonly="" class="copyable-inline-input" size="23" type="text" value="kubectl get deployments"></p>

<p>Expected sample output:</p>
<pre class="highlight plaintext"><code>deployment "redis-master" created
NAME           DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
redis-master   1         1         1            1           8m
</code><button class="button button--copy js-copy-button-9"><i class="fa fa-clipboard"></i></button></pre>
<h3>Step 4: create and deploy redis slave Service object</h3>

<p>We are using a custom-built Redis Docker image in our private repo that we created in an earlier step.</p>

<p><input readonly="" class="copyable-inline-input" size="18" type="text" value="cd $HOME/guestbook"></p>

<p>Similar to previous step, we create the slave service first:</p>

<p><input readonly="" class="copyable-inline-input" size="41" type="text" value="kubectl apply -f redis-slave-service.yaml"></p>

<p>Make sure service was created successfully:</p>

<p><input readonly="" class="copyable-inline-input" size="45" type="text" value="kubectl get service -l &quot;app=redis,role=slave&quot;"></p>

<p>Expected sample output:</p>
<pre class="highlight plaintext"><code>service "redis-slave" created
NAME          TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
redis-slave   ClusterIP   10.100.135.126   &lt;none&gt;        6379/TCP   8s
</code><button class="button button--copy js-copy-button-10"><i class="fa fa-clipboard"></i></button></pre>
<h3>Step 5: create and deploy redis slave Deployment object</h3>

<p>For this Deployment we want to reference the custom image we built and pushed into ECR from the previous lab. The deployment yaml file provided in the lab needs to be modified to know which ECR repo to use. So let’s replace it:</p>

<p>Note: Scroll the box below to the right to see the entire command.
<input readonly="" class="copyable-inline-input" size="109" type="text" value="sed  &quot;s@ECR_REDIS_REPO@${ECR_REDIS_REPO}@&quot; redis-slave-deployment.yaml > redis-slave-deployment-resolved.yaml"></p>

<p>Now we can send the object to Kubernetes:</p>

<p><input readonly="" class="copyable-inline-input" size="53" type="text" value="kubectl apply -f redis-slave-deployment-resolved.yaml"></p>

<p>Confirm deployment was successful:</p>

<p><input readonly="" class="copyable-inline-input" size="23" type="text" value="kubectl get deployments"></p>

<p>Expected sample output:</p>
<pre class="highlight plaintext"><code>deployment "redis-slave" created
NAME            DESIRED CURRENT UP-TO-DATE AVAILABLE AGE
redis-slave     2       2       2          2         40s
</code><button class="button button--copy js-copy-button-11"><i class="fa fa-clipboard"></i></button></pre>
<p>We can also make sure all the pods are up and running as well:</p>

<p><input readonly="" class="copyable-inline-input" size="16" type="text" value="kubectl get pods"></p>

<p>Expected sample output:</p>
<pre class="highlight plaintext"><code>NAME                          READY     STATUS    RESTARTS   AGE
redis-master-7898448-h6bct    1/1       Running   0          26m
redis-slave-d8866bcfb-6wfp4   1/1       Running   0          11s
redis-slave-d8866bcfb-xsx4z   1/1       Running   0          11s
</code><button class="button button--copy js-copy-button-12"><i class="fa fa-clipboard"></i></button></pre>
<h2 id="step7">Deploy Guestbook PHP Frontend App using Kubernetes</h2>

<h3>Step 1: create and deploy the front-end Service object</h3>

<p><input readonly="" class="copyable-inline-input" size="39" type="text" value="kubectl create -f frontend-service.yaml"></p>

<p>Confirm service was created successfully:</p>

<p><input readonly="" class="copyable-inline-input" size="38" type="text" value="kubectl get service -l &quot;app=guestbook&quot;"></p>

<p>Expected sample output:</p>
<pre class="highlight plaintext"><code>service "frontend" created
NAME       CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
frontend   10.59.249.26   &lt;pending&gt;     80:31936/TCP   8s
</code><button class="button button--copy js-copy-button-13"><i class="fa fa-clipboard"></i></button></pre>
<h3>Step 2: create and deploy the front-end Deployment object</h3>

<p>For this Deployment we want to reference the custom image we built and pushed into ECR from the previous lab. The deployment yaml file provided in the lab needs to be modified to know which ECR repo to use. So let’s replace it:</p>

<p>Note: Scroll the box below to the right to see the entire command.
<input readonly="" class="copyable-inline-input" size="110" type="text" value="sed &quot;s@ECR_GUESTBOOK_REPO@${ECR_GUESTBOOK_REPO}@&quot; frontend-deployment.yaml > frontend-deployment-resolved.yaml"></p>

<p>Now we can send the object to Kubernetes:</p>

<p><input readonly="" class="copyable-inline-input" size="50" type="text" value="kubectl apply -f frontend-deployment-resolved.yaml"></p>

<p>Confirm deployment was created successfully:</p>

<p><input readonly="" class="copyable-inline-input" size="42" type="text" value="kubectl get deployments -l &quot;app=guestbook&quot;"></p>

<p>Expected sample output:</p>
<pre class="highlight plaintext"><code>deployment "frontend" created
NAME       DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
frontend   3         3         3            3           12m
</code><button class="button button--copy js-copy-button-14"><i class="fa fa-clipboard"></i></button></pre>
<h3>Step 3: Retrieve front-end service Guestbook IP</h3>

<p>The remote Service Load Balancer IP can be retrieved by running:</p>

<p><input readonly="" class="copyable-inline-input" size="46" type="text" value="kubectl get service -l &quot;app=guestbook&quot; -o wide"></p>

<p>Expected sample output:</p>
<pre class="highlight plaintext"><code>NAME       TYPE           CLUSTER-IP      EXTERNAL-IP                                                              PORT(S)        AGE       SELECTOR
frontend   LoadBalancer   10.100.121.60   a9e724ed36d1611e8a673022ef4fadcc-327154510.us-west-2.elb.amazonaws.com   80:30055/TCP   4m        app=guestbook,tier=frontend
</code><button class="button button--copy js-copy-button-15"><i class="fa fa-clipboard"></i></button></pre>
<p>The <input readonly="" class="copyable-inline-input" size="11" type="text" value="EXTERNAL-IP"> field is the Load Balancer address. Access the application at <input readonly="" class="copyable-inline-input" size="20" type="text" value="http://<EXTERNAL-IP>"></p>

<p><img src="./Introduction to Containers and Kubernetes with EKS _ Qwiklabs + heptio_files/image4.png" alt="Functioning Guestbook Application"></p>

<p>We can also look at all pods running and see what particular worker node they are running on:</p>

<p><input readonly="" class="copyable-inline-input" size="16" type="text" value="kubectl get pods"></p>

<p>Notice how we have multiple copies (replicas) of some of the images/Deployments. Kubernetes handles all this creation and distribution around the cluster for us.</p>
<pre class="highlight plaintext"><code>NAME                          READY     STATUS    RESTARTS   AGE
frontend-66fc89995b-6zgxj     1/1       Running   0          2m
frontend-66fc89995b-7pspg     1/1       Running   0          2m
frontend-66fc89995b-xj6v9     1/1       Running   0          2m
redis-master-7898448-h6bct    1/1       Running   0          33m
redis-slave-d8866bcfb-6wfp4   1/1       Running   0          7m
redis-slave-d8866bcfb-xsx4z   1/1       Running   0          7m
</code><button class="button button--copy js-copy-button-16"><i class="fa fa-clipboard"></i></button></pre>
<p>At this point, we now have our application deployed into Kubernetes! If a node is lost or the state of the system changes, Kubernetes will make what changes it needs to get us back to that desired state.</p>

<h2 id="step8">Clean Up Your Lab Environment</h2>

<p>First let’s see how we can delete all resources in a given Kubernetes namespace:</p>

<p><input readonly="" class="copyable-inline-input" size="24" type="text" value="kubectl delete all --all"></p>

<p>Now let’s delete the EKS cluster</p>

<p><input readonly="" class="copyable-inline-input" size="50" type="text" value="eksctl delete cluster --cluster-name eks-cluster-1"></p>

<h1>Summary</h1>

<p>This completes the labs for the workshop, you have successfully containerized an multi-tier application and deployed it to a Kubernetes cluster on EKS! Feel free to continue to experiment and explore the lab environment as time permits. It will automatically terminate at the end of the workshop.</p>

</div>
