# EKS Getting Started

These are my personal notes on AWS EKS with EKSCLT and HEPTIO requirements spelled out

## Prerequisite Tools

* AWS CLI `pip install awscli`
  * [PyEnv](https://github.com/pyenv/pyenv)
    * macOS: `brew install pyenv`
  * Python3
    * pyenv: `pyenv install 3.6.2`
* Docker
  * Docker Machine
    * macOS: `brew install docker-machine`
* Eksctl
* Kubectl
  * macOS: `brew install kubectl`

Obviously, Linux users do not need Docker-Machine and can run docker natively.  Alternatively to Docker-Machine, you can use Docker-for-Mac or Docker-for-Windows.

### EKSCTL

```bash
EKSCTL_URL="https://github.com/weaveworks/eksctl/releases/download/latest_release/eksctl_$(uname -s)_amd64.tar.gz"
curl --silent --location "${EKSCTL_URL}" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin
```

### Heptio Authenticator

```bash
go get -u -v github.com/heptio/authenticator/cmd/heptio-authenticator-aws
```

You can also download it directly:

* Linux: https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-06-05/bin/linux/amd64/heptio-authenticator-aws
* macOS: https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-06-05/bin/darwin/amd64/heptio-authenticator-aws
* Windows: https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-06-05/bin/windows/amd64/heptio-authenticator-aws.exe

```bash
HEPTIO_S3_URL="https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-06-05/bin/darwin/amd64/heptio-authenticator-aws"
curl -o heptio-authenticator-aws ${HEPTIO_S3_URL}
chmod +x ./heptio-authenticator-aws
cp heptio-authenticator-aws /usr/local/bin # assumes homebrew environment
```

## Creating the Cluster

```bash
export EKS_CLUSTER_NAME="mycluster"
eksctl create cluster \
 --cluster-name ${EKS_CLUSTER_NAME} \
 --kubeconfig ~/.kube/config \
 --nodes 3 \
 --ssh-public-key ~/.ssh/${EKS_CLUSTER_NAME}.id_rsa.pub
```
