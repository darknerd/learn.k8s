# Lab 01: Containerize and Publish Applications

In this lab, you will set up your lab workstation, create and publish Docker images for a two-tier web application, and start the process of creating a Kubernetes cluster using Amazon's Elastic Container Service for Kubernetes (EKS).

## Workstation Setup

### Step 1: Access Your Lab Workstation

There are two ways to access your lab workstation. You can use the web terminal built into Strigo by clicking the My Lab icon on the left:

* Strigo Lab Workstation

Or you can issue the 'lab-info' command in the web terminal to retrieve SSH connection details.

### Step 2: Configure the AWS CLI

We will use the AWS cli to set up authentication for EKS cluster creation, ECR authentication, and to create ECR repositories.

Set up your AWS credentials by running 'aws configure'. When prompted for your 'AWS Access Key ID' and 'AWS Secret Access Key', enter the values displayed in Qwiklabs console. When prompted for 'Default region name', enter 'us-west-2'.


```bash
$ aws configure

AWS Access Key ID [None]: <copy Access Key ID from Qwiklabs>
AWS Secret Access Key [None]: <copy Secret Access Key from Qwiklabs>
Default region name [None]: us-west-2
Default output format [None]: <leave blank>
```

##  Setting Up a Container Registry
Your Kubernetes cluster needs a container registry to pull images from. Here we'll use Amazon's Elastic Container Registry (ECR). We will create two repositories in ECR, one for the frontend guestbook application and one for the Redis backend.

###  Step 1: Create an ECR repository

```bash
aws ecr create-repository --repository-name redis-slave
aws ecr create-repository --repository-name guestbook
```

Example output:

```json
{
    "repository": {
        "repositoryArn": "arn:aws:ecr:us-west-2:883456218093:repository/guestbook",
        "repositoryUri": "883456210000.dkr.ecr.us-west-2.amazonaws.com/guestbook",
        "registryId": "883456210000",
        "createdAt": 1528415168.0,
        "repositoryName": "nginx"
    }
}
```

"repositoryURI" is the URI for the repository. You will use this to push and pull your Docker images later in the lab.

### Step 2: Store the repository URI as an environment variable
To make it easier to reference our repositories later on, assign the names to environment variables.

Note: Scroll the box below to the right to see the entire command.

```bash
export ECR_REDIS_REPO=$(aws ecr describe-repositories --repository-names "redis-slave" --query 'repositories[*].[repositoryUri]' --output text)

export ECR_GUESTBOOK_REPO=$(aws ecr describe-repositories --repository-names "guestbook" --query 'repositories[*].[repositoryUri]' --output text)
```

We can confirm the creation of these repositories by browsing to ECR in the Amazon Console

### Step 3: Setup Docker to ECR authentication
We need to connect Docker to our AWS account. Use the following aws cli command to login. Be sure to include the ‘$’ at the beginning.

```bash
$(aws ecr get-login --no-include-email)
```

Expected output:

```
Login Succeeded
```

## Build and Publish Docker Image for the Backend Application

### Step 1: Fetch the lab files

We have prepared an archive with the various files we will be using for the lab. Let’s fetch a copy and unpack it.

Note: Scroll the box below to the right to see the entire command.

```bash
wget https://s3-us-west-2.amazonaws.com/heptio-training/k8sintro/guestbook.tar.gz
tar -zxvf guestbook.tar.gz
```

### Step 2: Build redis-slave Docker image

We've provided a complete Dockerfile that you'll use to build the image. Feel free to view the Dockerfile to see what is being run.


```bash
cd $HOME/guestbook/redis-slave
```

Build and tag the image. Do not forget the "." at the end.


```bash
docker build -t redis-slave:v1 .
```

Watch the output of this command and notice the various stages that happen during the build process of an image. Next we will add another tag to the image. This will allow us to push the image to ECR


```bash
docker tag redis-slave:v1 ${ECR_REDIS_REPO}:v1
```

We can then verify this applied by listing the images. Notice how even those we have two entries, the IMAGE ID is the same for both.


`docker images`

```bash
$ docker images
REPOSITORY                                                 TAG                 IMAGE ID            CREATED              SIZE
908357542759.dkr.ecr.us-west-2.amazonaws.com/redis-slave   v1                  c3ec44a1eac5        About a minute ago   107 MB
redis-slave                                                v1                  c3ec44a1eac5        About a minute ago   107 MB
redis                                                      latest              bfcb1f6df2db        5 weeks ago          107 MB
```

###  Step 3: Push the Redis image to the Elastic Container Registry (ECR)
Since we already added the tag and connected Docker to ECR, we can simply run 'docker push image_name' and our image will be uploaded to ECR.


```bash
docker push ${ECR_REDIS_REPO}
```

### Step 4: Verify the image has been published to the registry

We can browse to the ECR repo in the Amazon Console and verify that it was uploaded:

Redis in ECR

## Build and Publish Docker Image for the Frontend Application
We are now going to repeat image creation and publishing process, this time for the PHP guestbook application. Many of the same principles and explanations apply here. The Dockerfile will be different however so make sure to take a look at it.

### Step 1: Build the Guestbook PHP application Docker image

```bash
cd $HOME/guestbook/guestbook
```

Build and tag the image. Do not forget the "." at the end.


```bash
docker build -t guestbook:v1 .
```

### Step 2: Push the Guestbook image to the Elastic Container Registry (ECR)

Just like before, we will add another tag to the image. This will allow us to push the image to ECR.


```bash
docker tag guestbook:v1 ${ECR_GUESTBOOK_REPO}:v1
```

And now push the image to ECR.

```bash
docker push ${ECR_GUESTBOOK_REPO}
```

### Step 3: Verify the image has been published to the registry

We can browse to ECR repo in the Amazon Console and verify that it was uploaded

Guestbook in ECR

Now that you have uploaded your images to ECR, your Kubernetes cluster will be able to pull them when we deploy the application in the next lab.

## Create a Kubernetes Cluster using Amazon Elastic Container Service for Kubernetes (EKS)
So far, we have not used Kubernetes. We have containerized the applications and published images to a container registry (ECR) for later use. In lab 02 we will deploy our application to Kubernetes. To conclude lab 01, we'll start the EKS cluster creation process so that it can run while we continue with the course.

### Step 1: Create an SSH Keypair for EKS cluster nodes
Create a ssh keypair that will be used for the Kubernetes worker nodes.


```bash
ssh-keygen -q -N "" -f ~/.ssh/id_rsa
```

Example output:

```bash
Generating public/private rsa key pair.
Enter file in which to save the key (/home/ubuntu/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/ubuntu/.ssh/id_rsa.
Your public key has been saved in /home/ubuntu/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:Sw46wvE8V7iDwXJrOM1+ceRkgvmMs+/hEbZLIDxIuYE ubuntu@ip-172-31-47-156
The key\'s randomart image is:
+---[RSA 2048]----+
|                 |
|. .              |
|E+   o           |
|.oo.o ..+        |
|..* =+=*S        |
| . %oB+Oo.       |
|  = %oOoo        |
|   =.*.=         |
|    .+=          |
+----[SHA256]-----+
```


### Step 2: Create the cluster
For this class, we will use the eksctl command-line tool to setup our EKS cluster. For an in depth look at the EKS cluster creation process, check out this EKS blog post.

Note: Scroll the box below to the right to see the entire command.

```bash
eksctl create cluster --cluster-name eks-cluster-1 --kubeconfig $HOME/.kube/config
```

Example output:

```
2018-06-07T20:53:53Z [ℹ]  importing SSH public key "/home/ubuntu/.ssh/id_rsa.pub" as "EKS-floral-gopher-1528404833"
2018-06-07T20:53:54Z [ℹ]  creating EKS cluster "floral-gopher-1528404833"
2018-06-07T20:53:54Z [ℹ]  creating VPC stack "EKS-floral-gopher-1528404833-VPC"
2018-06-07T20:53:54Z [ℹ]  creating ServiceRole stack "EKS-floral-gopher-1528404833-ServiceRole"
2018-06-07T20:54:35Z [✔]  created ServiceRole stack "EKS-floral-gopher-1528404833-ServiceRole"
2018-06-07T20:54:54Z [✔]  created VPC stack "EKS-floral-gopher-1528404833-VPC"
2018-06-07T20:54:54Z [ℹ]  creating control plane "floral-gopher-1528404833"
2018-06-07T21:03:57Z [✔]  created control plane "floral-gopher-1528404833"
2018-06-07T21:03:57Z [ℹ]  creating DefaultNodeGroup stack "EKS-floral-gopher-1528404833-DefaultNodeGroup"
2018-06-07T21:07:38Z [✔]  created DefaultNodeGroup stack "EKS-floral-gopher-1528404833-DefaultNodeGroup"
2018-06-07T21:07:38Z [✔]  all EKS cluster "floral-gopher-1528404833" resources has been created
2018-06-07T21:07:38Z [ℹ]  wrote "kubeconfig"
2018-06-07T21:07:39Z [ℹ]  the cluster has 0 nodes
2018-06-07T21:07:39Z [ℹ]  waiting for at least 2 nodes to become ready
2018-06-07T21:08:10Z [ℹ]  the cluster has 2 nodes
2018-06-07T21:08:10Z [ℹ]  node "ip-192-168-255-151.us-west-2.compute.internal" is ready
2018-06-07T21:08:10Z [ℹ]  node "ip-192-168-78-184.us-west-2.compute.internal" is ready
```

The above command will take about 15 minutes to complete. While it is executing you will see the various actions being taken and can view them in the AWS Console. For example:

* [Roles](https://console.aws.amazon.com/iam/home?region=us-west-2#/roles)
* [VPC](https://us-west-2.console.aws.amazon.com/vpc/home?region=us-west-2#vpcs:)
* [EKS Cluster](https://us-west-2.console.aws.amazon.com/eks/home?region=us-west-2#/)

Allow the cluster creation process to complete while we move forward with the lecture on Kubernetes. We will return here for Lab 02 following the lecture.

# Lab 02 - Deploy Applications using Kubernetes
## Deploy Redis using Kubernetes
### Step 1: make sure the cluster is up and healthy
At this point, we're ready for deployment using Kubernetes. Access to kubernetes is primarily done via the [command line tool kubectl](https://kubernetes.io/docs/reference/kubectl/overview/) which has already been installed on the remote you’re logged into.
`kubectl` is invoking HTTP REST JSON requests against the Kubernetes API server. It was pre-configured as the last step of the eksctl command we ran at the end of lab 01. Let’s first make sure connectivity is established by listing the available nodes in the cluster.


`kubectl get nodes`

Example output:

```
NAME                                            STATUS    ROLES     AGE       VERSION
ip-192-168-188-54.us-west-2.compute.internal    Ready     <none>    1m        v1.10.3
ip-192-168-201-201.us-west-2.compute.internal   Ready     <none>    1m        v1.10.3
```
The version number may vary slightly from what is shown above. The clusters that are created use the default version for EKS. We can also look at the configuration file for kubectl that tells it how to talk to the cluster:


`kubectl config view`

Example output:

```yaml
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: REDACTED
    server: https://F43F2B7C7A570E6ACB99167C007B502A.yl4.us-west-2.eks.amazonaws.com
  name: eks-cluster-1.us-west-2.eksctl.io
contexts:
- context:
    cluster: eks-cluster-1.us-west-2.eksctl.io
    user: awsstudent@eks-cluster-1.us-west-2.eksctl.io
  name: awsstudent@eks-cluster-1.us-west-2.eksctl.io
current-context: awsstudent@eks-cluster-1.us-west-2.eksctl.io
kind: Config
preferences: {}
users:
- name: awsstudent@eks-cluster-1.us-west-2.eksctl.io
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      args:
      - token
      - -i
      - eks-cluster-1
      command: heptio-authenticator-aws
      env: null
```

Notice the API url of the control plane and the authentication being done with the Heptio Authenticator for AWS. We can run the command manually to see that it gives kubectl a token that it can send along to the API server which is how the authentication is performed:

`heptio-authenticator-aws token -i eks-cluster-1`

### Step 2: create and deploy redis master Service object
We are using a pre-built Redis Docker image.


`cd $HOME/guestbook`

We are now going to use the kubectl command to send up YAML files representing the desired state we would like Kubernetes to get the cluster to. We have pre-authored these manifest files, but please feel free to view them and research any parts of them you are curious about.


`kubectl apply -f redis-master-service.yaml`

Confirm redis master service is running by fetching details about we just sent into Kubernetes. If we see an entry and an IP address was created for it, then we are good!


`kubectl get service redis-master`

Expected sample output:

```
NAME           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
redis-master   ClusterIP   10.100.106.40   <none>        6379/TCP   31s]
```

### Step 3: create and deploy redis master Deployment object
We are using a pre-built Redis Docker image.


`cd $HOME/guestbook`

Create redis master Deployment using manifest:


`kubectl apply -f redis-master-deployment.yaml`

Confirm redis master Deployment is running:


`kubectl get deployments`

Expected sample output:

```bash
deployment "redis-master" created
NAME           DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
redis-master   1         1         1            1           8m
```

### Step 4: create and deploy redis slave Service object
We are using a custom-built Redis Docker image in our private repo that we created in an earlier step.


`cd $HOME/guestbook`

Similar to previous step, we create the slave service first:


`kubectl apply -f redis-slave-service.yaml`

Make sure service was created successfully:


`kubectl get service -l "app=redis,role=slave"`

Expected sample output:

```
service "redis-slave" created
NAME          TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
redis-slave   ClusterIP   10.100.135.126   <none>        6379/TCP   8s
```

### Step 5: create and deploy redis slave Deployment object
For this Deployment we want to reference the custom image we built and pushed into ECR from the previous lab. The deployment yaml file provided in the lab needs to be modified to know which ECR repo to use. So let’s replace it:

Note: Scroll the box below to the right to see the entire command.

```bash
sed  "s@ECR_REDIS_REPO@${ECR_REDIS_REPO}@" redis-slave-deployment.yaml > redis-slave-deployment-resolved.yaml
```

Now we can send the object to Kubernetes:


`kubectl apply -f redis-slave-deployment-resolved.yaml`

Confirm deployment was successful:


`kubectl get deployments`

Expected sample output:

```bash
$ deployment "redis-slave" created
NAME            DESIRED CURRENT UP-TO-DATE AVAILABLE AGE
redis-slave     2       2       2          2         40s
```

We can also make sure all the pods are up and running as well:


`kubectl get pods`

Expected sample output:

```
NAME                          READY     STATUS    RESTARTS   AGE
redis-master-7898448-h6bct    1/1       Running   0          26m
redis-slave-d8866bcfb-6wfp4   1/1       Running   0          11s
redis-slave-d8866bcfb-xsx4z   1/1       Running   0          11s
```

## Deploy Guestbook PHP Frontend App using Kubernetes
### Step 1: create and deploy the front-end Service object

`kubectl create -f frontend-service.yaml`

Confirm service was created successfully:


`kubectl get service -l "app=guestbook"`

Expected sample output:

```bash
service "frontend" created
NAME       CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
frontend   10.59.249.26   <pending>     80:31936/TCP   8s
```

### Step 2: create and deploy the front-end Deployment object
For this Deployment we want to reference the custom image we built and pushed into ECR from the previous lab. The deployment yaml file provided in the lab needs to be modified to know which ECR repo to use. So let’s replace it:

Note: Scroll the box below to the right to see the entire command.

```bash
sed "s@ECR_GUESTBOOK_REPO@${ECR_GUESTBOOK_REPO}@" frontend-deployment.yaml > frontend-deployment-resolved.yaml
```

Now we can send the object to Kubernetes:


`kubectl apply -f frontend-deployment-resolved.yaml`

Confirm deployment was created successfully:


`kubectl get deployments -l "app=guestbook"`

Expected sample output:

```bash
deployment "frontend" created
NAME       DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
frontend   3         3         3            3           12m
```

### Step 3: Retrieve front-end service Guestbook IP
The remote Service Load Balancer IP can be retrieved by running:


`kubectl get service -l "app=guestbook" -o wide`

Expected sample output:

```
NAME       TYPE           CLUSTER-IP      EXTERNAL-IP                                                              PORT(S)        AGE       SELECTOR
frontend   LoadBalancer   10.100.121.60   a9e724ed36d1611e8a673022ef4fadcc-327154510.us-west-2.elb.amazonaws.com   80:30055/TCP   4m        app=guestbook,tier=frontend
```

The
EXTERNAL-IP
 field is the Load Balancer address. Access the application at
http://<EXTERNAL-IP>

Functioning Guestbook Application

We can also look at all pods running and see what particular worker node they are running on:

`kubectl get pods`

Notice how we have multiple copies (replicas) of some of the images/Deployments. Kubernetes handles all this creation and distribution around the cluster for us.

```
NAME                          READY     STATUS    RESTARTS   AGE
frontend-66fc89995b-6zgxj     1/1       Running   0          2m
frontend-66fc89995b-7pspg     1/1       Running   0          2m
frontend-66fc89995b-xj6v9     1/1       Running   0          2m
redis-master-7898448-h6bct    1/1       Running   0          33m
redis-slave-d8866bcfb-6wfp4   1/1       Running   0          7m
redis-slave-d8866bcfb-xsx4z   1/1       Running   0          7m
```
At this point, we now have our application deployed into Kubernetes! If a node is lost or the state of the system changes, Kubernetes will make what changes it needs to get us back to that desired state.

## Clean Up Your Lab Environment
First let’s see how we can delete all resources in a given Kubernetes namespace:


`kubectl delete all --all`

Now let’s delete the EKS cluster

`eksctl delete cluster --cluster-name eks-cluster-1`

## Summary
This completes the labs for the workshop, you have successfully containerized an multi-tier application and deployed it to a Kubernetes cluster on EKS! Feel free to continue to experiment and explore the lab environment as time permits. It will automatically terminate at the end of the workshop.
